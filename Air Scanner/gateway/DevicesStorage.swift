//
//  MapDevicesStorage.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 15.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation
import Combine
import FirebaseFirestore

final class DevicesStorage: ObservableObject {
    static let shared = DevicesStorage()
    @Published var publicDevices: [Device] = []
    @Published var userDevices: [Device] = []
    @Published var selectedMetric: PublicMetric? = nil
    
    private var publicDevicesListener: ListenerRegistration?
    private var userDevicesListener: ListenerRegistration?
    
    var filteredDevices: [Device] {
        let uniqueDevices = Array(Set(publicDevices + userDevices))
        guard let selectedMetric = selectedMetric else { return uniqueDevices }
        return uniqueDevices.filter { $0.settings.publicMetrics.contains(selectedMetric) }
    }
    
    init() {
        subscribe()
    }
    
    deinit {
        unsubscribe()
    }
    
    func updateSelectedMetric(_ metric: PublicMetric?) {
        selectedMetric = metric
    }
    
    private func unsubscribe() {
        publicDevices = []
        publicDevicesListener?.remove()
        publicDevicesListener = nil
        unsubscribeFormUserSpecifcInfo()
    }
    
    func unsubscribeFormUserSpecifcInfo() {
        userDevices = []
        userDevicesListener?.remove()
        userDevicesListener = nil
    }
    
    func subscribeForUserSpecificInfo() {
        guard let userId = LocalStorage.shared.user?.id else { return }
        userDevicesListener?.remove()
        
        userDevicesListener = Firestore.firestore().collection("devices").whereField("user_id", isEqualTo: userId).addSnapshotListener{ snapshot, error in
            guard let snapshot = snapshot else { return }
            for change in snapshot.documentChanges {
                let document = change.document
                switch change.type {
                case .added:
                    self.userDevices.append(Device(id: document.documentID, data: document.data()))
                case .removed:
                    if let index = self.userDevices.firstIndex(where: { $0.id == document.documentID }) {
                        self.userDevices.remove(at: index)
                    }
                case .modified:
                    if let index = self.userDevices.firstIndex(where: { $0.id == document.documentID }) {
                        self.userDevices[index].configure(with: document.data())
                    }
                }
            }
        }
    }
    
    private func subscribe() {
        publicDevicesListener?.remove()
        publicDevicesListener = Firestore.firestore().collection("devices").whereField("hasPublicMetrics", isEqualTo: true).addSnapshotListener { snapshot, error in
            guard let snapshot = snapshot else { return }
            for change in snapshot.documentChanges {
                let document = change.document
                switch change.type {
                case .added:
                    self.publicDevices.append(Device(id: document.documentID, data: document.data()))
                case .removed:
                    if let index = self.publicDevices.firstIndex(where: { $0.id == document.documentID }) {
                        self.publicDevices.remove(at: index)
                    }
                case .modified:
                    if let index = self.publicDevices.firstIndex(where: { $0.id == document.documentID }) {
                        self.publicDevices[index].configure(with: document.data())
                    }
                }
            }
        }
        subscribeForUserSpecificInfo()
    }
}
