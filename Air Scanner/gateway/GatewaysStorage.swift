//
//  GatewaysStorage.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 27.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation
import Combine
import FirebaseFirestore

final class GatewaysStorage: ObservableObject {
    static let shared = GatewaysStorage()
    @Published var gateways: [Gateway] = []
    
    private var listener: ListenerRegistration?
    
    init() {
        subscribe()
    }
    
    deinit {
        unsubscribe()
    }
    
    private func unsubscribe() {
        unsubscribeFormUserSpecifcInfo()
    }
    
    func unsubscribeFormUserSpecifcInfo() {
        gateways = []
        listener?.remove()
        listener = nil
    }
    
    func subscribeForUserSpecificInfo() {
        guard let userId = LocalStorage.shared.user?.id else { return }
        listener?.remove()
        listener = Firestore.firestore().collection("gateways").whereField("user_id", isEqualTo: userId).addSnapshotListener { snapshot, error in
            guard let snapshot = snapshot else { return }
            for change in snapshot.documentChanges {
                let document = change.document
                switch change.type {
                case .added:
                    self.gateways.append(Gateway(id: document.documentID, data: document.data()))
                case .removed:
                    if let index = self.gateways.firstIndex(where: { $0.id == document.documentID }) {
                        self.gateways.remove(at: index)
                    }
                case .modified:
                    if let index = self.gateways.firstIndex(where: { $0.id == document.documentID }) {
                        self.gateways[index].configure(with: document.data())
                    }
                }
            }
        }
    }
    
    private func subscribe() {
        subscribeForUserSpecificInfo()
    }
}
