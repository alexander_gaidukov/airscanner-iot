//
//  SettingsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 09.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct SettingsGatewayCell: View {
    @ObservedObject var gateway: Gateway
    var body: some View {
        HStack(spacing: 10) {
            ZStack {
                Circle()
                    .fill(Color.mainButton)
                    .frame(width: 48, height: 48)
                Image("gateway")
                    .offset(x: 16, y: 0)
            }
            .padding(.horizontal, 11)
            Text(gateway.name)
                .font(.system(size: 14, weight: .bold))
            Spacer()
            Image(systemName: "chevron.right")
                .font(.system(size: 14))
                .padding(.trailing, 20)
        }
        .frame(height: 70)
        .background(Color.inputBackground)
        .cornerRadius(2)
        .foregroundColor(.white)
    }
}

struct SettingsDeviceCell: View {
    @ObservedObject var device: Device
    var body: some View {
        HStack(spacing: 10) {
            ZStack {
                Circle()
                    .fill(Color.mainButton)
                    .frame(width: 48, height: 48)
                Image("device")
                    .offset(x: 16, y: 0)
            }
            .padding(.horizontal, 11)
            VStack(alignment: .leading, spacing: 5) {
                Text(device.settings.displayName)
                    .font(.system(size: 14, weight: .bold))
                Text(device.address)
                    .font(.system(size: 12))
                    .foregroundColor(.secondaryText)
            }
            Spacer()
            Image(systemName: "chevron.right")
                .font(.system(size: 14))
                .padding(.trailing, 20)
        }
        .frame(height: 70)
        .background(Color.inputBackground)
        .cornerRadius(2)
        .foregroundColor(.white)
    }
}

private struct GatewaysSectionView: View {
    @EnvironmentObject private var gatewaysStorage: GatewaysStorage
    @State var selectedGateway: Gateway? = nil
    
    private func gatewayLinkActivation(to gateway: Gateway) -> Binding<Bool> {
        Binding<Bool>(get: { gateway == self.selectedGateway }, set: { self.selectedGateway = $0 ? gateway : nil })
    }
    
    var body: some View {
        ForEach(Array(gatewaysStorage.gateways.enumerated()), id: \.offset) { gateway in
            ZStack {
                SettingsGatewayCell(gateway: gateway.element)
                NavigationLink(destination: GatewayDetailsView().environmentObject(gateway.element), isActive: self.gatewayLinkActivation(to: gateway.element)) {
                    EmptyView()
                }
                .padding(.trailing, 20)
            }
            .listRowInsets(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 0))
        }
    }
}

private struct DevicesSectionView: View {
    var isPublic: Bool
    @EnvironmentObject private var devicesStorage: DevicesStorage
    @State var selectedDevice: Device? = nil
    
    private func deviceLinkActivation(to device: Device) -> Binding<Bool> {
        Binding<Bool>(get: { device == self.selectedDevice }, set: { self.selectedDevice = $0 ? device : nil })
    }
    
    var body: some View {
        ForEach(Array((isPublic ? devicesStorage.publicDevices : devicesStorage.userDevices.filter{ $0.settings.gatewayId.isEmpty }).enumerated()), id: \.offset) { device in
            ZStack {
                SettingsDeviceCell(device: device.element)
                NavigationLink(destination: DeviceDetailsView().deviceNavigationView().environmentObject(device.element), isActive: self.deviceLinkActivation(to: device.element)) {
                    EmptyView()
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.trailing, 20)
            }
            .listRowInsets(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 0))
        }
    }
}

private struct UserInfoView: View {
    @EnvironmentObject var localStorage: LocalStorage
    var body: some View {
        HStack(spacing: 15) {
            RemoteImage(placeholder: Image(systemName: "person.fill"), url: localStorage.user?.photoURL)
                .aspectRatio(contentMode: .fit)
                .foregroundColor(.white)
                .frame(width: 60, height: 60)
                .cornerRadius(30)
            VStack(alignment: .leading) {
                Text(localStorage.user?.fullName ?? "")
                    .font(.system(size: 24, weight: .semibold))
                    .foregroundColor(.white)
                Text(localStorage.user?.userName ?? "")
                    .font(.system(size: 13))
                    .foregroundColor(.white)
            }
        }
    }
}

struct SettingsView: View {
    @State private var askForLogout = false
    @Binding var selectedTab: Int
    @State var listType: Int = 0
    
    var listTypes = ["My Devices", "Public Devices"]
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .topLeading) {
                Color.background.edgesIgnoringSafeArea(.all)
                VStack(alignment: .leading, spacing: 25) {
                    UserInfoView()
                    Picker(selection: self.$listType, label: EmptyView()) {
                        ForEach(0..<self.listTypes.count) {
                            Text(self.listTypes[$0])
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    
                    Group {
                        if listType == 0 {
                            List {
                                GatewaysSectionView()
                                DevicesSectionView(isPublic: false)
                                
                                Button(action: {
                                    self.selectedTab = 1
                                }) {
                                    HStack {
                                        Spacer()
                                        Text("+ Add new")
                                            .font(.system(size: 15, weight: .bold))
                                            .foregroundColor(.white)
                                        Spacer()
                                    }
                                    .padding(20)
                                }
                                .listRowInsets(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                            }
                            .background(Color.background)
                        } else {
                            List {
                                DevicesSectionView(isPublic: true)
                            }
                            .background(Color.background)
                        }
                    }
                    
                    HStack(spacing: 20) {
                        Button(action: { }) {
                            HStack(spacing: 15) {
                                Image("privacy")
                                Text("Privacy")
                                    .font(.system(size: 14))
                            }
                            .foregroundColor(.white)
                            .padding()
                        }
                        .frame(height: 60)
                        .frame(maxWidth: .infinity)
                        
                        Button(action: { self.askForLogout = true }) {
                            HStack(spacing: 15) {
                                Image("exit")
                                Text("Sign out")
                                    .font(.system(size: 14))
                            }
                            .foregroundColor(.white)
                            .padding()
                        }
                        .frame(height: 60)
                        .frame(maxWidth: .infinity)
                    }
                    .background(Color.mainButton)
                    .cornerRadius(3)
                }
                .padding()
            }
            .navigationBarTitle("Profile")
        }
        .overlay(
            LoginOverlayView()
        )
        .alert(isPresented: $askForLogout) {
            return Alert(title: Text("Do you really want to sign out?"), message: nil, primaryButton: .cancel(), secondaryButton: .default(Text("Sign out"), action: {
                withAnimation {
                    Authentication.signOut()
                }
            }))
        }
    }
}
