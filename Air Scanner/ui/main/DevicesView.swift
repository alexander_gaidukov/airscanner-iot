//
//  DevicesView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 09.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct DevicesView: View {
    @State private var objectType: Int = 0
    @State private var gatewayState = GatewayState()
    @State private var deviceState = DeviceState()
    @Environment(\.alertState) var alertState
    var gatewayEndpoint = Endpoint<EmptyResponse>(baseURL: API.baseURL, path: "gateway", method: .post)
    var deviceEndpoint = Endpoint<EmptyResponse>(baseURL: API.baseURL, path: "device", method: .post)
    
    @State private var isLoading: Bool = false
    
    var objectTypes = ["Device", "Gateway"]
    
    var body: some View {
        NavigationView {
            GeometryReader { proxy in
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 0) {
                        ZStack(alignment: .topLeading) {
                            Color.background.edgesIgnoringSafeArea(.all)
                            VStack(spacing: 36) {
                                Picker(selection: self.$objectType, label: EmptyView()) {
                                    ForEach(0..<self.objectTypes.count) {
                                        Text(self.objectTypes[$0])
                                    }
                                }.pickerStyle(SegmentedPickerStyle())
                                Group {
                                    if self.objectType == 0 {
                                        NewDeviceView(state: self.$deviceState)
                                    } else {
                                        NewGatewayView(state: self.$gatewayState)
                                    }
                                }
                                Color.background.frame(width: 40, height: 60)
                            }
                            .padding()
                            VStack {
                                Spacer()
                                Button(action: {
                                    self.createDevice()
                                }) {
                                    ZStack {
                                        if (!self.isLoading) {
                                            Text(self.objectType == 0 ? "Add device" : "Add gateway")
                                        }
                                        LoadingIndicator(isLoading: self.isLoading, color: .white)
                                    }
                                }
                                .buttonStyle(MainButtonStyle())
                                .disabled((self.objectType == 0 && !self.deviceState.isValid) || (self.objectType == 1 && !self.gatewayState.isValid))
                                .frame(height: 40)
                                .frame(maxWidth: .infinity)
                                .padding(.bottom)
                            }.padding()
                        }
                        .frame(minHeight: proxy.size.height)
                        .disabled(self.isLoading)
                        Color.background.frame(width: proxy.size.width, height: proxy.safeAreaInsets.bottom)
                    }
                }
                .edgesIgnoringSafeArea(.bottom)
            }
            .navigationBarTitle("Add new")
        }
        .onDisappear() {
            self.gatewayEndpoint.result = nil
            self.deviceEndpoint.result = nil
        }
        .onReceive(gatewayEndpoint.$result) { result in
            self.isLoading = false
            switch result {
            case .success?:
                self.showAlertWithState(.success("The gateway was added. Now you can track it."))
                self.gatewayState.clear()
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
        .onReceive(deviceEndpoint.$result) { result in
            self.isLoading = false
            switch result {
            case .success?:
                self.showAlertWithState(.success("The device was added. Now you can track it."))
                self.deviceState.clear()
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
        .overlay(
            LoginOverlayView()
        )
    }
    
    private func showAlertWithState(_ state: AlertState) {
        withAnimation {
            self.alertState.wrappedValue = state
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + AlertView.displayTime) {
            withAnimation{
                self.alertState.wrappedValue = .dismissed
            }
        }
    }
    
    private func createDevice() {
        isLoading = true
        if objectType == 0 {
            deviceState.makeRequest { self.deviceEndpoint.load(params: $0) }
        } else {
            gatewayEndpoint.load(params: gatewayState.request)
        }
    }
}

struct DevicesView_Previews: PreviewProvider {
    static var previews: some View {
        DevicesView()
    }
}
