
//
//  SwiftUIView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 19.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

enum AlertState {
    case dismissed
    case success(String)
    case error(String)
    
    var isPresented: Bool {
        if case .dismissed = self { return false }
        return true
    }
}

struct AlertView: View {
    
    static let displayTime: TimeInterval = 2
    
    var state: AlertState
    
    var message: String? {
        switch state {
        case .dismissed:
            return nil
        case .success(let message):
            return message
        case .error(let message):
            return message
        }
    }
    
    var title: String? {
        switch state {
        case .dismissed:
            return nil
        case .success:
            return "Success!"
        case .error:
            return "Oops!"
        }
    }
    
    var color: Color {
        switch state {
        case .dismissed:
            return .clear
        case .success:
            return .successAlert
        case .error:
            return .errorAlert
        }
    }
    
    var image: Image {
        switch state {
        case .dismissed:
            return Image(systemName: "")
        case .success:
            return Image(systemName: "checkmark.circle.fill")
        case .error:
            return Image(systemName: "multiply.circle.fill")
        }
    }
    
    var body: some View {
        ZStack {
            Color
                .black
                .opacity(0.3)
                .edgesIgnoringSafeArea(.all)
                .zIndex(0)
            VStack {
                Spacer()
                image
                    .font(.system(size: 80))
                Spacer()
                VStack(spacing: 10) {
                    Text(title ?? "")
                        .font(.system(size: 16, weight: .bold))
                    Text(message ?? "")
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                        .font(.system(size: 14, weight: .semibold))
                }
                .foregroundColor(.white)
                Spacer()
            }
            .padding(.horizontal, 20)
            .frame(width: 300, height: 285)
            .background(color)
            .cornerRadius(4)
        }
    }
}

struct AlertPresentedView<Content: View>: View {
    @State var alertState: AlertState = .dismissed
    var content: Content
    var body: some View {
        content
            .overlay (
               Group {
                   if alertState.isPresented {
                       AlertView(state: alertState)
                           .transition(.opacity)
                   }
               }
            )
            .environment(\.alertState, $alertState)
    }
}

extension View {
    func alertPresentation() -> some View {
        AlertPresentedView(content: self)
    }
}
