//
//  Colors.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 29.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

extension Color {
    static let background = Color("Background")
    static let mainButton = Color("MainButton")
    static let mainButtonPressed = Color("MainButtonPressed")
    static let darkText = Color("DarkText")
    static let inputBackground = Color("InputBackground")
    static let successAlert = Color("SuccessAlert")
    static let errorAlert = Color("ErrorAlert")
    static let secondaryText = Color("SecondaryText")
    static let destroy = Color("destroy")
}
