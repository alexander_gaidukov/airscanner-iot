//
//  Utils.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 08.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct EnvironmentWindowKey: EnvironmentKey {
    static var defaultValue: UIWindow? = nil
}

extension EnvironmentValues {
    var window: UIWindow? {
        get {
            self[EnvironmentWindowKey.self]
        }
        
        set {
            self[EnvironmentWindowKey.self] = newValue
        }
    }
}

///Device Info view style

struct EnvironmentDeviceViewStyleKey: EnvironmentKey {
    static var defaultValue: DeviceInfoViewStyle = .map
}

extension EnvironmentValues {
    var deviceViewStyle: DeviceInfoViewStyle {
        get {
            self[EnvironmentDeviceViewStyleKey.self]
        }
        
        set {
            self[EnvironmentDeviceViewStyleKey.self] = newValue
        }
    }
}

struct DeviceInfoViewStyleModifier: ViewModifier {
    var style: DeviceInfoViewStyle
    func body(content: Content) -> some View {
        content.environment(\.deviceViewStyle, style)
    }
}

extension View {
    func infoViewStyle(_ style: DeviceInfoViewStyle) -> some View {
        self.modifier(DeviceInfoViewStyleModifier(style: style))
    }
}

///Alert State
struct EnvironmentAlertStateKey: EnvironmentKey {
    static var defaultValue: Binding<AlertState> = .constant(.dismissed)
}

extension EnvironmentValues {
    var alertState: Binding<AlertState> {
        get {
            self[EnvironmentAlertStateKey.self]
        }
        
        set {
            self[EnvironmentAlertStateKey.self] = newValue
        }
    }
}
