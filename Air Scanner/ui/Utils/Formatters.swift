//
//  Formatters.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 29.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct Formatters {
    static let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 0
        return formatter
    }()
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }()
}
