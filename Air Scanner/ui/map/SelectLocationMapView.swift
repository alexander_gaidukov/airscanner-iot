//
//  SelectLocationMapView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 18.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct SelectLocationMapView : UIViewControllerRepresentable {
    @EnvironmentObject var locationManager: LocationManager
    @Binding var location: Location?
    
    func makeUIViewController(context: Context) -> MapViewController {
        let controller =  MapViewController()
        controller.userLocation = locationManager.location
        return controller
    }
    
    func updateUIViewController(_ uiViewController:  MapViewController, context: Context) {
        uiViewController.userLocation = locationManager.location
        uiViewController.allowLocationSelection = true
        uiViewController.configure(with: location)
        uiViewController.didSelectLocation = {
            self.location = $0
        }
    }
}
