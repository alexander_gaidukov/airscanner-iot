//
//  MapView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 15.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct MapView: View {
    @State private var moveToUserLocation: Bool = false
    @State private var showFilters: Bool = false
    @State private var selectedDevice: Device? = nil
    var body: some View {
        ZStack(alignment: .topLeading) {
            WhirlyGlobeMapView(moveToUserLocation: $moveToUserLocation,
                               selectedDevice: $selectedDevice)
                .edgesIgnoringSafeArea(.top)
                .zIndex(0)
            HStack {
                Button(action: {
                    withAnimation {
                        self.showFilters = true
                    }
                }) { Image("filters").padding() }
                Spacer()
                Button(action: {
                    self.moveToUserLocation = true
                }) { Image("location").padding() }
            }
            .zIndex(1)
            
            if showFilters || selectedDevice != nil {
                Color
                    .black
                    .opacity(0.3)
                    .edgesIgnoringSafeArea(.all)
                    .zIndex(2)
                    .transition(.opacity).onTapGesture {
                        withAnimation {
                            self.showFilters = false
                            self.selectedDevice = nil
                        }
                    }
                
                if showFilters {
                    FiltersView(showFilters: $showFilters)
                        .frame(width: 200)
                        .zIndex(3)
                        .transition(.move(edge: .leading))
                } else {
                    ZStack(alignment: .bottomLeading) {
                        Color.clear
                        DeviceInfoView(device: selectedDevice!, shouldFilterMetrics: true, shouldBottomOffset: true)
                            .infoViewStyle(.map)
                            .frame(height: 320)
                            .cornerRadius(4)
                            .offset(x: 0, y: 64)
                    }
                    .zIndex(3)
                    .transition(.move(edge: .bottom))
                }
            }
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
