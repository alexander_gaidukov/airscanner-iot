//
//  WhirlyGlobeMapView.swift
//  Air Scanner
//
//  Created by User on 06.05.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation
import SwiftUI



struct WhirlyGlobeMapView : UIViewControllerRepresentable {
    
    private static var mapViewController: MapViewController? = nil
    
    @EnvironmentObject private var storage: DevicesStorage
    @EnvironmentObject private var locationManager: LocationManager
    @Binding var moveToUserLocation: Bool
    @Binding var selectedDevice: Device?
    
    func makeUIViewController(context: Context) -> MapViewController {
        
        // Workaround of the well known bug of SwiftUI. It recreates the VC every time we change the tab in TabView
        if WhirlyGlobeMapView.mapViewController == nil {
            let controller = MapViewController()
            controller.userLocation = locationManager.location
            WhirlyGlobeMapView.mapViewController = controller
        }
        
        return WhirlyGlobeMapView.mapViewController!
    }
    
    func updateUIViewController(_ uiViewController:  MapViewController, context: Context) {
        uiViewController.configure(with: storage.filteredDevices, selectedDevice: selectedDevice)
        uiViewController.userLocation = locationManager.location
        uiViewController.didSelectDevice = { device in
            withAnimation {
                self.selectedDevice = device
            }
        }
        if moveToUserLocation {
            uiViewController.moveToUserLocation()
            DispatchQueue.main.async {
                self.moveToUserLocation = false
            }
        }
    }
}


struct WhirlyGlobeMapView_Previews: PreviewProvider {
    @State static var moveToUserLocation: Bool = false
    @State static var selectedDevice: Device? = nil
    static var previews: some View {
        WhirlyGlobeMapView(moveToUserLocation: $moveToUserLocation, selectedDevice: $selectedDevice)
    }
}
