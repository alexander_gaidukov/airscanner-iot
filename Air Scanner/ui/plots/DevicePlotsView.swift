//
//  DevicePlotsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 02.07.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

enum DevicePlotsViewType: String, CaseIterable {
    case hour
    case today
    case week
}

struct DevicePlotsView: View {
    @EnvironmentObject var device: Device
    @ObservedObject var weekEndpoint: Endpoint<MetricsHistory> = Endpoint()
    @ObservedObject var hourEndpoint: Endpoint<MetricsHistory> = Endpoint()
    @ObservedObject var todayEndpoint: Endpoint<MetricsHistory> = Endpoint()
    @State var plotTypeIndex = 0
    
    var body: some View {
        VStack(spacing: 20) {
            Picker(selection: $plotTypeIndex, label: EmptyView()) {
                ForEach(Array(DevicePlotsViewType.allCases.enumerated()), id: \.offset){
                    Text($0.element.rawValue.capitalized).frame(height: 65)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            Group {
                if plotTypeIndex == 0 {
                    DevicePlotsCollectionView(history: hourEndpoint.value)
                } else if plotTypeIndex == 1 {
                    DevicePlotsCollectionView(history: todayEndpoint.value)
                } else {
                    DevicePlotsCollectionView(history: weekEndpoint.value)
                }
            }
        }
        .onAppear() {
            self.loadData()
        }
    }
    
    private func loadData() {
        weekEndpoint.configure(baseURL: API.metricsURL, path: "device/\(device.id)/lastWeek", method: .get, params: ["smooth": 2, "metrics": device.sortedMetricKeys])
        weekEndpoint.decodeFromSnakeCase = false
        weekEndpoint.load(params: EmptyRequest())
        
        hourEndpoint.configure(baseURL: API.metricsURL, path: "device/\(device.id)/lastHour", method: .get, params: ["smooth": 2, "metrics": device.sortedMetricKeys])
        hourEndpoint.decodeFromSnakeCase = false
        hourEndpoint.load(params: EmptyRequest())
        
        todayEndpoint.configure(baseURL: API.metricsURL, path: "device/\(device.id)/lastDay", method: .get, params: ["smooth": 2, "metrics": device.sortedMetricKeys])
        todayEndpoint.decodeFromSnakeCase = false
        todayEndpoint.load(params: EmptyRequest())
    }
}

private struct DevicePlotsCollectionView: View {
    var history: MetricsHistory?
    var body: some View {
        Group {
            if self.history == nil {
                ZStack {
                    Color.clear
                    LoadingIndicator(isLoading: true, color: .white)
                }
            } else if self.history!.isEmpty {
                ZStack {
                    Color.clear
                    Text("No metrics available")
                        .font(.system(size: 17, weight: .bold))
                        .foregroundColor(.secondaryText)
                }
            } else {
                GeometryReader { proxy in
                    ScrollView(showsIndicators: false) {
                        self.elements(for: proxy.size.width)
                    }
                }
            }
        }
    }
    
    private func elements(for width: CGFloat) -> some View {
        let space: CGFloat = 10
        let size = round((width - space) / 2.0)
        let offset = width - size * 2
        let elements = history!.history.keys.count
        let rows = (elements / 2) + (elements % 2)
        let cols = 2
        return VStack(spacing: offset) {
            ForEach(0..<rows) { row in
                HStack(spacing: offset) {
                    ForEach(0..<cols) { col in
                        Group {
                            if self.keyValue(at: row, col: col) != nil {
                                DevicePlotCell(self.keyValue(at: row, col: col)!)
                                    .frame(width: size, height: size)
                                    .background(Color.inputBackground)
                                    .cornerRadius(4)
                            } else {
                                Spacer()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func keyValue(at row: Int, col: Int) -> (key: String, value: [MetricsRecord])? {
        let index = row * 2 + col
        guard index < history!.history.keys.count else { return nil }
        let key = Array(history!.history.keys)[index]
        let value = history!.history[key]!
        return (key, value)
    }
}

extension CGSize {
    init(point: CGPoint) {
        self.init(width: point.x, height: point.y)
    }
}

private struct DevicePlotCell: View {
    @EnvironmentObject var device: Device
    var key: String
    var history: [MetricsRecord]
    init(_ entry: (key: String, value: [MetricsRecord])) {
        self.key = entry.key
        self.history = entry.value
    }
    var body: some View {
        ZStack(alignment: .topLeading){
            Color.clear
            VStack(alignment: .leading, spacing: 8) {
                VStack(alignment: .leading, spacing: 0) {
                    Text(device.settings.config[key]?.metricType?.rawValue ?? key)
                        .font(.system(size: 14, weight: .bold))
                        .foregroundColor(.white)
                    Text(device.settings.config[key]?.metricType == nil ? " " : "Normal range: " + device.settings.config[key]!.metricType!.normalRange.label)
                        .font(.system(size: 10))
                        .foregroundColor(Color.white.opacity(0.7))
                }
                ZStack {
                    Color.red
                }
            }
            .padding(10)
        }
    }
}
