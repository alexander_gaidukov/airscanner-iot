//
//  DeviceMetricsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 24.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct MetricCell: View {
    var metricItem: DeviceMetricItem
    
    private var color: LinearGradient {
        guard let metric = metricItem.metricConfig?.metricType else { return LinearGradient(gradient: Gradient(colors: [Color.clear]), startPoint: .top, endPoint: .bottom) }
        let gradient: Gradient
        switch metric {
        case .co2:
            gradient = Gradient(colors: [Color("co2_start"), Color("co2_end")])
        case .humidity:
             gradient = Gradient(colors: [Color("humidity_start"), Color("humidity_end")])
        case .pm2_5, .pm1_0, .pm10:
            gradient = Gradient(colors: [Color("pm_start"), Color("pm_end")])
        case .temperature:
            gradient = Gradient(colors: [Color("temperature_start"), Color("temperature_end")])
        }
        return LinearGradient(gradient: gradient, startPoint: .top, endPoint: .bottom)
    }
    
    var body: some View {
        ZStack {
            Color.clear
            VStack {
                VStack(spacing: 2) {
                    Text(metricItem.metricConfig?.metricType?.rawValue ?? metricItem.key)
                        .font(.system(size: 14, weight: .bold))
                        .foregroundColor(.white)
                    Text(metricItem.metricConfig?.metricType == nil ? " " : "Normal range: " + metricItem.metricConfig!.metricType!.normalRange.label)
                        .font(.system(size: 10))
                        .foregroundColor(Color.white.opacity(0.7))
                }
                Spacer()
                PieChartView(value: metricItem.value, range: metricItem.metricConfig?.metricType?.normalRange, color: color)
                    .frame(width: 80, height: 80)
            }
            .padding(.vertical, 10)
        }
    }
}

struct Arc: Shape {
    var angle: Double
    
    var animatableData: Double {
        get { angle }
        set { angle = newValue }
    }

    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.addArc(center: CGPoint(x: rect.midX, y: rect.midY), radius: rect.width / 2, startAngle: Angle(degrees: 0), endAngle: Angle(degrees: angle), clockwise: false)

        return path
    }
}

struct PieChartView: View {
    var value: Double
    var range: ClosedRange<Int>?
    var color: LinearGradient
    
    @Environment(\.deviceViewStyle) var style: DeviceInfoViewStyle
    @State var appeared: Bool = false
    
    init(value: Double, range: ClosedRange<Int>?, color: LinearGradient) {
        self.value = value
        self.range = range
        self.color = color
    }
    
    var endAngle: Double {
        guard let range = range else { return 0 }
        let v = min(max((value - Double(range.lowerBound)) / Double(range.upperBound - range.lowerBound) ,0), 1)
        return 360.0 * v
    }
    
    var body: some View {
        GeometryReader { proxy in
            ZStack {
                Text("\(Formatters.numberFormatter.string(from: NSNumber(value: self.value)) ?? "0")")
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(.white)
                Circle()
                    .stroke(self.style.circleBackground, lineWidth: 6)
                    .frame(width: proxy.size.width, height: proxy.size.height)
                Arc(angle: self.appeared ? self.endAngle : 0)
                    .stroke(self.color, lineWidth: 6)
                    .frame(width: proxy.size.width, height: proxy.size.height)
                    .animation(.default)
                    .rotationEffect(Angle(degrees: -90))
            }
        }.onAppear() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.appeared = true
            }
        }
    }
}

struct DeviceMetricsView: View {
    @Environment(\.deviceViewStyle) var style: DeviceInfoViewStyle
    @EnvironmentObject var device: Device
    @EnvironmentObject var storage: DevicesStorage
    var shouldFilterMetrics: Bool
    var body: some View {
        ZStack {
            if device.metrics.isEmpty {
                HStack(alignment: .center) {
                    Spacer()
                    Text("No metrics available")
                        .font(.system(size: 17, weight: .bold))
                        .foregroundColor(.secondaryText)
                    Spacer()
                }
                .frame(height: 154)
            } else {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 10) {
                        ForEach(Array(self.device.metricItems.filter { !self.shouldFilterMetrics || self.storage.selectedMetric == nil || $0.metricConfig?.metricType == self.storage.selectedMetric }.enumerated()), id: \.offset) { item in
                            MetricCell(metricItem: item.element)
                                .frame(width: 154)
                                .background(self.style.cellBackground)
                                .cornerRadius(4)
                        }
                    }
                }.frame(height: 154)
            }
        }
    }
}
