//
//  DeviceInfoView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 24.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

enum DeviceInfoViewStyle {
    case map
    case details
    
    var background: Color {
        switch self {
        case .map:
            return Color.background
        case .details:
            return Color.inputBackground
        }
    }
    
    var cellBackground: Color {
        switch self {
        case .map:
            return Color.inputBackground
        case .details:
            return Color.background
        }
    }
    
    var circleBackground: Color {
        switch self {
        case .map:
            return Color.background
        case .details:
            return Color.inputBackground
        }
    }
}

struct DeviceInfoView: View {
    @Environment(\.deviceViewStyle) var style: DeviceInfoViewStyle
    @ObservedObject var device: Device
    var shouldFilterMetrics: Bool = false
    var shouldBottomOffset: Bool = false
    var body: some View {
        ZStack(alignment: .topLeading) {
            Color.clear
            VStack(alignment: .leading, spacing: 5) {
                Text(device.settings.displayName)
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(.white)
                HStack(alignment: .top, spacing: 8) {
                    Image("pin_icon")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 12)
                    Text(device.address)
                        .font(.system(size: 14))
                        .foregroundColor(Color.white.opacity(0.6))
                }
            }
            .padding(16)
            
            DeviceMetricsView(shouldFilterMetrics: shouldFilterMetrics)
                .environmentObject(device)
                .padding(.horizontal, 16)
                .padding(.top, 80)
        }
        .background(style.background)
    }
    
    
}
