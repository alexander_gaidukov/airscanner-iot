//
//  DeviceDetailsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 22.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

private struct DeviceDetailsGeneralInfoView: View {
    @EnvironmentObject var device: Device
    @Binding var copied: Bool
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            
            HStack(alignment: .top, spacing: 8) {
                Image("pin_icon")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 12)
                Text(device.address)
                    .font(.system(size: 14))
                Spacer()
            }
            
            if device.isOwnedByCurrentUser {
                Button(action: {self.copyToClipboard(self.device.clientId)}) {
                    HStack(alignment: .center, spacing: 5) {
                        Text("ClientId: " + device.clientId)
                        Spacer()
                        Image(systemName: "doc.on.doc.fill")
                    }
                }
                .font(Font.system(size: 14).italic())
                
                Button(action: {self.copyToClipboard(self.device.topic)}) {
                    HStack(alignment: .center, spacing: 5) {
                        Text("Topic: " + device.topic)
                        Spacer()
                        Image(systemName: "doc.on.doc.fill")
                    }
                }
                .font(Font.system(size: 14).italic())
            }
        }
        .foregroundColor(Color.white)
    }
    
    private func copyToClipboard(_ text: String) {
        UIPasteboard.general.string = text
        copied = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.copied = false
        }
    }
}

private struct DeviceDetailsNavigationItems: View {
    @EnvironmentObject private var device: Device
    var body: some View {
        Group {
            if device.isOwnedByCurrentUser {
                NavigationLink(destination: UpdateDeviceSettingsView()) {
                    Image(systemName: "gear")
                        .foregroundColor(.white)
                        .font(.system(size: 20))
                        .padding()
                }.isDetailLink(false)
            }
        }
    }
}

struct DeviceDetailsView: View {
    @State var copied = false
    var body: some View {
        ZStack(alignment: .topLeading) {
            Color.background
            VStack(spacing: 20) {
                DeviceDetailsGeneralInfoView(copied: $copied)
                DevicePlotsView()
            }
            .padding()
        }
        .navigationBarItems(trailing:
            DeviceDetailsNavigationItems()
        )
        .overlay(
            ZStack {
                if copied {
                    Text("Copied")
                        .padding()
                        .font(.system(size: 14, weight: .medium))
                        .foregroundColor(.white)
                        .background(Color.mainButton.opacity(0.3))
                        .cornerRadius(2)
                }
            }
        )
    }
}

struct DeviceNavigationView<Content: View>: View {
    @EnvironmentObject var device: Device
    var content: Content
    var body: some View {
        content
            .navigationBarTitle(device.settings.displayName)
    }
}

extension View {
    func deviceNavigationView() -> some View {
        return DeviceNavigationView(content: self)
    }
}
