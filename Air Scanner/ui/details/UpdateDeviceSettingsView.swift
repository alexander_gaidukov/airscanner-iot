//
//  UpdateDeviceSettingsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 29.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct UpdateDeviceSettingsView: View {
    @EnvironmentObject var device: Device
    @Environment(\.alertState) var alertState
    var removeEndpoint: Endpoint<EmptyResponse> = Endpoint()
    var endpoint: Endpoint<EmptyResponse> = Endpoint()
    
    @State private var isUpdating: Bool = false
    @State private var isDeleting: Bool = false
    @State private var askForRemoving = false
    @State private var deviceState: UpdateDeviceState = UpdateDeviceState()
    @State private var configured = false
    
    var body: some View {
       GeometryReader { proxy in
           ScrollView(showsIndicators: false) {
               VStack(spacing: 0) {
                   ZStack(alignment: .topLeading) {
                       Color.background.edgesIgnoringSafeArea(.all)
                        VStack(spacing: 36) {
                            UpdateDeviceView(state: self.$deviceState)
                            Color.background.frame(width: 40, height: 120)
                        }
                        .padding()
                        VStack(spacing: 20) {
                           Spacer()
                           Button(action: {
                               self.updateDevice()
                           }) {
                               ZStack {
                                    if (!self.isUpdating) {
                                        Text("Update device")
                                    }
                                    LoadingIndicator(isLoading: self.isUpdating, color: .white)
                               }
                           }
                           .buttonStyle(MainButtonStyle())
                           .disabled(!self.deviceState.isValid)
                           .frame(height: 40)
                           .frame(maxWidth: .infinity)
                            
                            Button(action: {
                                self.askForRemoving = true
                            }) {
                                ZStack {
                                    if (!self.isDeleting) {
                                        Text("Remove device")
                                    }
                                    LoadingIndicator(isLoading: self.isDeleting, color: .white)
                                }
                            }
                            .buttonStyle(DestroyButtonStyle())
                            .frame(height: 40)
                            .frame(maxWidth: .infinity)
                        
                       }.padding()
                   }
                   .frame(minHeight: proxy.size.height)
                   .disabled(self.isUpdating || self.isDeleting)
                   Color.background.frame(width: proxy.size.width, height: proxy.safeAreaInsets.bottom)
               }
           }
           .edgesIgnoringSafeArea(.bottom)
       }
       .navigationBarTitle("Edit Device")
       .onAppear() {
            self.configureState()
       }
       .onDisappear() {
            self.endpoint.result = nil
            self.removeEndpoint.result = nil
       }
       .onReceive(removeEndpoint.$result) { result in
            self.isDeleting = false
            switch result {
            case .success?:
                break
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
        .onReceive(endpoint.$result) { result in
            self.isUpdating = false
            switch result {
            case .success?:
                self.showAlertWithState(.success("The device was updated."))
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
       .alert(isPresented: $askForRemoving) {
            return Alert(title: Text("Do you really want to remove the device?"), message: nil, primaryButton: .cancel(), secondaryButton: .destructive(Text("Remove"), action: {
                self.removeDevice()
            }))
        }
    }
    
    private func updateDevice() {
        isUpdating = true
        endpoint.configure(baseURL: API.baseURL, path: "device/\(device.id)", method: .put)
        endpoint.encodeToSnakeCase = false
        deviceState.makeRequest { self.endpoint.load(params: $0) }
    }
    
    private func removeDevice() {
        isDeleting = true
        removeEndpoint.configure(baseURL: API.baseURL, path: "device/\(device.id)", method: .delete)
        removeEndpoint.load(params: EmptyRequest())
    }
    
    private func configureState() {
        guard !configured else { return }
        deviceState.configure(device: device)
        configured = true
    }
    
    private func showAlertWithState(_ state: AlertState, completion: (() -> ())? = nil) {
        withAnimation {
            self.alertState.wrappedValue = state
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + AlertView.displayTime) {
            withAnimation{
                self.alertState.wrappedValue = .dismissed
            }
            completion?()
        }
    }
}

struct UpdateDeviceSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateDeviceSettingsView()
    }
}
