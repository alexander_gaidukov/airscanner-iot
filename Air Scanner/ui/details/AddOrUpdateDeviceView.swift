//
//  AddOrUpdateDeviceView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 25.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

enum AddOrUpdateDeviceViewType {
    case addDevice
    case updateGateway
    
    var title: String {
        switch self {
        case .addDevice:
            return "Add Device"
        case .updateGateway:
            return "Edit Gateway"
        }
    }
    
    var addButton: String {
        switch self {
        case .addDevice:
            return "Add Device"
        case .updateGateway:
            return "Update Gateway"
        }
    }
    
    var destroyButton: String {
        switch self {
        case .addDevice:
            return ""
        case .updateGateway:
            return "Remove Gateway"
        }
    }
    
    var message: String {
        switch self {
        case .addDevice:
            return "The device was added. Now you can track it."
        case .updateGateway:
            return "The gateway was updated."
        }
    }
    
    var removePromptMessage: String {
        switch self {
        case .addDevice:
            return ""
        case .updateGateway:
            return "Do you really want to remove the gateway?"
        }
    }
    
    var isGateway: Bool {
        return self == .updateGateway
    }
    
    var isUpdate: Bool {
        return self == .updateGateway
    }
}

struct AddOrUpdateDeviceView: View {
    var type: AddOrUpdateDeviceViewType
    @State private var deviceState: DeviceState = DeviceState(showGateway: false)
    @State private var gatewayState: GatewayState = GatewayState(isUpdate: true)
    var endpoint: Endpoint<EmptyResponse> = Endpoint()
    var removeEndpoint: Endpoint<EmptyResponse> = Endpoint()
    
    @State private var isUpdating: Bool = false
    @State private var isDeleting: Bool = false
    @State private var askForRemoving = false
    @State private var configured = false
    
    @EnvironmentObject var gateway: Gateway
    @Environment(\.alertState) var alertState
    
    var body: some View {
       GeometryReader { proxy in
           ScrollView(showsIndicators: false) {
               VStack(spacing: 0) {
                   ZStack(alignment: .topLeading) {
                       Color.background.edgesIgnoringSafeArea(.all)
                       VStack(spacing: 36) {
                            Group {
                                if self.type.isGateway {
                                    NewGatewayView(state: self.$gatewayState)
                                } else {
                                    NewDeviceView(state: self.$deviceState)
                                }
                            }
                            Color.background.frame(width: 40, height: 120)
                       }
                       .padding()
                        VStack(spacing: 20) {
                           Spacer()
                           Button(action: {
                               self.createOrUpdateDevice()
                           }) {
                               ZStack {
                                    if (!self.isUpdating) {
                                        Text(self.type.addButton)
                                    }
                                    LoadingIndicator(isLoading: self.isUpdating, color: .white)
                               }
                           }
                           .buttonStyle(MainButtonStyle())
                           .disabled((self.type.isGateway && !self.gatewayState.isValid) || (!self.type.isGateway && !self.deviceState.isValid))
                           .frame(height: 40)
                           .frame(maxWidth: .infinity)
                            
                            if self.type.isUpdate {
                                Button(action: {
                                    self.askForRemoving = true
                                }) {
                                    ZStack {
                                        if (!self.isDeleting) {
                                            Text(self.type.destroyButton)
                                        }
                                        LoadingIndicator(isLoading: self.isDeleting, color: .white)
                                    }
                                }
                                .buttonStyle(DestroyButtonStyle())
                                .frame(height: 40)
                                .frame(maxWidth: .infinity)
                            }
                        
                       }.padding()
                   }
                   .frame(minHeight: proxy.size.height)
                   .disabled(self.isUpdating || self.isDeleting)
                   Color.background.frame(width: proxy.size.width, height: proxy.safeAreaInsets.bottom)
               }
           }
           .edgesIgnoringSafeArea(.bottom)
       }
       .navigationBarTitle(Text(self.type.title))
       .onAppear() {
            self.configureState()
       }
        .onReceive(endpoint.$result) { result in
            self.isUpdating = false
            switch result {
            case .success?:
                self.showAlertWithState(.success(self.type.message))
                self.deviceState.clear()
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
        .onReceive(removeEndpoint.$result) { result in
            self.isDeleting = false
            switch result {
            case .success?:
                self.endpoint.result = nil
            case .failure(let error)?:
                self.showAlertWithState(.error(error.localizedDescription))
            case nil:
                break
            }
        }
        .onDisappear() {
             self.endpoint.result = nil
             self.removeEndpoint.result = nil
        }
        .alert(isPresented: $askForRemoving) {
            return Alert(title: Text(self.type.removePromptMessage), message: nil, primaryButton: .cancel(), secondaryButton: .destructive(Text("Remove"), action: {
                self.removeDevice()
            }))
        }
    }
    
    private func createOrUpdateDevice() {
        isUpdating = true
        switch type {
        case .addDevice:
            endpoint.configure(baseURL: API.baseURL, path: "device", method: .post)
            deviceState.makeRequest { self.endpoint.load(params: $0) }
        case .updateGateway:
            endpoint.configure(baseURL: API.baseURL, path: "gateway/\(gateway.id)", method: .put)
            endpoint.load(params: gatewayState.request)
        }
    }
    
    private func removeDevice() {
        isDeleting = true
        removeEndpoint.configure(baseURL: API.baseURL, path: "gateway/\(gateway.id)", method: .delete)
        removeEndpoint.load(params: EmptyRequest())
    }
    
    private func configureState() {
        
        guard !configured else { return }
        
        switch type {
        case .addDevice:
            deviceState.gateway = gateway
            
        case .updateGateway:
            gatewayState.name = gateway.name
        }
        configured = true
    }
    
    private func showAlertWithState(_ state: AlertState, completion: (() -> ())? = nil) {
        withAnimation {
            self.alertState.wrappedValue = state
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + AlertView.displayTime) {
            withAnimation{
                self.alertState.wrappedValue = .dismissed
            }
            completion?()
        }
    }
}
