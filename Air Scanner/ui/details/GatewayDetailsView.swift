//
//  GatewayDetailsView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 22.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct GatewayDeviceDetailsCell: View {
    var device: Device
    var body: some View {
        DeviceInfoView(device: device)
            .infoViewStyle(.details)
            .frame(height: 250)
            .cornerRadius(4)
            .padding(EdgeInsets(top: 5, leading: 16, bottom: 5, trailing: 16))
    }
}

private struct DevicesListView: View {
    @EnvironmentObject private var storage: DevicesStorage
    @EnvironmentObject var gateway: Gateway
    @State var selectedDevice: Device? = nil
    
    private func deviceLinkActivation(to device: Device) -> Binding<Bool> {
        Binding<Bool>(get: { self.selectedDevice == device }, set: { self.selectedDevice = $0 ? device : nil })
    }
    
    var body: some View {
        ForEach(Array(self.storage.userDevices.filter { $0.settings.gatewayId == self.gateway.id }.enumerated()), id: \.offset) { item in
            NavigationLink(destination: DeviceDetailsView().deviceNavigationView().environmentObject(item.element), isActive: self.deviceLinkActivation(to: item.element)) {
                GatewayDeviceDetailsCell(device: item.element)
            }
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct GatewayDetailsView: View {
    @EnvironmentObject var gateway: Gateway
    var body: some View {
        GeometryReader { proxy in
            ScrollView {
                VStack(spacing: 0) {
                    DevicesListView()
                    NavigationLink(destination: AddOrUpdateDeviceView(type: .addDevice)) {
                        HStack {
                            Spacer()
                            Text("+ Add device")
                                .font(.system(size: 15, weight: .bold))
                                .foregroundColor(.white)
                            Spacer()
                        }
                        .padding(20)
                    }.isDetailLink(false)

                    Spacer()
                }
                .frame(minHeight: proxy.size.height)
                .background(Color.background)
            }
        }
        .navigationBarTitle("\(gateway.name)")
        .navigationBarItems(trailing:
            NavigationLink(destination: AddOrUpdateDeviceView(type: .updateGateway)) {
                Image(systemName: "gear")
                    .foregroundColor(.white)
                    .font(.system(size: 20))
                    .padding()
            }.isDetailLink(false)
        )
    }
}
