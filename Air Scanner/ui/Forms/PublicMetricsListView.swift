//
//  PublicMetricsListView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 02.07.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct PublicMetricListCell: View {
    var metric: PublicMetric?
    var selected: Bool
    
    var body: some View {
        HStack(spacing: 8) {
            Circle()
                .stroke(lineWidth: 2.0)
                .frame(width: 16, height: 16)
                .overlay(
                    ZStack {
                        if selected {
                            Circle()
                                .fill()
                                .frame(width: 12, height: 12)
                        }
                    }
            ).foregroundColor(.white)
            Text(metric?.rawValue ?? "None")
                .font(.system(size: 17))
                .foregroundColor(.white)
        }
        .padding()
    }
}

struct PublicMetricsListView: View {
    @Binding var isDisplayed: Bool
    @Binding var config: MetricConfig
    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            
            List {
                Button(action: {
                    self.config.metricType = nil
                    self.config.isPublic = false
                    self.isDisplayed = false
                }) {
                    PublicMetricListCell(metric: nil, selected: self.config.metricType == nil)
                }
                ForEach(Array(PublicMetric.allCases.enumerated()), id: \.offset) { item in
                    Button(action: {
                        self.config.metricType = item.element
                        self.isDisplayed = false
                    }) {
                        PublicMetricListCell(metric: item.element, selected: item.element == self.config.metricType)
                    }
                }
            }
        }.navigationBarTitle("Measurements", displayMode: .inline)
    }
}
