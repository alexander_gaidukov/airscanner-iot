//
//  NewDeviceView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 29.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct DeviceState {
    var showGateway: Bool = true
    var name: String = ""
    var gateway: Gateway? = nil
    var location: Location? = nil
    var dataFormatIndex: Int = 0
    var key: String = ""
    
    var locationDescription: String {
        get { location.map { ["\($0.lat)", "\($0.lon)"].joined(separator: " ") } ?? "" }
        set {  }
    }
    
    var dataFormat: DeviceDataFormat {
        DeviceDataFormat.allCases[dataFormatIndex]
    }
    
    var isValid: Bool {
        !name.isEmpty && location != nil && (gateway != nil || !key.isEmpty)
    }
    
    func makeRequest(completion: @escaping (AddDeviceRequest) -> ()) {
        Geocoder.shared.loadAddress(from: location!) { address in
            let key = self.gateway == nil ? self.key : ""
            let request = AddDeviceRequest(key: key,
                                           gatewayId: self.gateway?.id ?? "",
                                           displayName: self.name,
                                           isPublic: false,
                                           locLat: self.location?.lat ?? 0,
                                           locLon: self.location?.lon ?? 0,
                                           dataFormat: self.dataFormat,
                                           locationDescription: address?.address ?? "")
            completion(request)
        }
    }
    
    mutating func clear() {
        name = ""
        location = nil
        dataFormatIndex = 0
        key = ""
        if showGateway {
            gateway = nil
        }
     }
}

struct NewDeviceView: View {
    @Binding var state: DeviceState
    @State var readQRCode: Bool = false
    @State var selectGateway: Bool = false
    @State var selectLocation: Bool = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            VStack(alignment: .leading, spacing: 2) {
                Text("Device's name")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                TextField("Type name...", text: $state.name)
                    .textFieldStyle(MainTextFieldStyle())
            }
            
            if state.showGateway {
                VStack(alignment: .leading, spacing: 2) {
                    Text("Gateway")
                        .font(.system(size: 14))
                        .foregroundColor(.white)
                    NavigationLink(destination: GatewayListView(isDisplayed: $selectGateway, gateway: $state.gateway), isActive: $selectGateway) {
                        Text(state.gateway != nil ? state.gateway!.name : "Gateway")
                    }
                        .buttonStyle(DisclosureButtonStyle())
                        .foregroundColor(.inputBackground)
                        .frame(height: 36.0)
                        .frame(maxWidth: .infinity)
                }
            }
            
            if state.gateway == nil {
                VStack(alignment: .leading, spacing: 2) {
                    Text("Key")
                        .font(.system(size: 14))
                        .foregroundColor(.white)
                    NavigationLink(destination: QRCodeReaderView(isDisplayed: $readQRCode, token: $state.key), isActive: $readQRCode) {
                        Text(state.key.isEmpty ? "Scan the QR code" : state.key)
                    }
                        .buttonStyle(DisclosureButtonStyle())
                        .foregroundColor(.inputBackground)
                        .frame(height: 36.0)
                        .frame(maxWidth: .infinity)
                }
            }
            
            VStack(alignment: .leading, spacing: 2) {
                Text("Location")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                NavigationLink(destination: LocationSelectionView(location: $state.location, isDisplayed: $selectLocation), isActive: $selectLocation) {
                    HStack(spacing: 15) {
                        TextField("Select your location", text: $state.locationDescription)
                            .textFieldStyle(MainTextFieldStyle())
                            .disabled(true)
                        Image("pin_icon")
                            .foregroundColor(.white)
                    }
                }
            }
            
            VStack(alignment: .leading, spacing: 2) {
                Text("Data format")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                Picker(selection: $state.dataFormatIndex, label: EmptyView()) {
                    ForEach(0..<DeviceDataFormat.allCases.count) {
                        Text(DeviceDataFormat.allCases[$0].displayName).frame(height: 80)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
        }
    }
}
