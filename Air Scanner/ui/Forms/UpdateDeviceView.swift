//
//  UpdateDeviceView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 30.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct UpdateDeviceState {
    var name: String = ""
    var gateway: Gateway? = nil
    var location: Location? = nil
    var dataFormatIndex: Int = 0
    var key: String = ""
    var config: [String: MetricConfig] = [:]
    
    private var deviceLocation: Location? = nil
    private var deviceLocationDescription: String = ""
    
    init() {
        
    }
    
    init(device: Device) {
        self.configure(device: device)
    }
    
    mutating func configure(device: Device) {
        name = device.settings.displayName
        gateway = GatewaysStorage.shared.gateways.first { $0.id == device.settings.gatewayId }
        location = device.settings.location
        dataFormatIndex = DeviceDataFormat.allCases.firstIndex(of: device.settings.dataFormat) ?? 0
        deviceLocation = device.settings.location
        deviceLocationDescription = device.settings.locationDescription
        config = device.extendedConfig
    }
    
    var locationDescription: String {
        get { location.map { ["\($0.lat)", "\($0.lon)"].joined(separator: " ") } ?? "" }
        set {  }
    }
    
    var dataFormat: DeviceDataFormat {
        DeviceDataFormat.allCases[dataFormatIndex]
    }
    
    var isValid: Bool {
        !name.isEmpty && location != nil && (gateway != nil || !key.isEmpty)
    }
    
    func makeRequest(completion: @escaping (UpdateDeviceRequest) -> ()) {
        
        func request(address: String?) -> UpdateDeviceRequest {
            let key = self.gateway == nil ? self.key : ""
            return UpdateDeviceRequest(key: key,
                                    gateway_id: self.gateway?.id ?? "",
                                    display_name: self.name,
                                    is_public: false,
                                    loc_lat: self.location?.lat ?? 0,
                                    loc_lon: self.location?.lon ?? 0,
                                    data_format: self.dataFormat,
                                    location_description: address ?? "",
                                    metricsConfig: self.config.filter { $1.metricType != nil })
        }
        
        if location?.lat == deviceLocation?.lat && location?.lon == deviceLocation?.lon && !deviceLocationDescription.isEmpty {
            completion(request(address: deviceLocationDescription))
            return
        }
        
        Geocoder.shared.loadAddress(from: location!) { address in
            completion(request(address: address?.address))
        }
    }
}

private struct DeviceMetricCell: View {
    var key: String
    @Binding var config: MetricConfig
    @State var expanded: Bool = false
    @State var selectMeasurement: Bool = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Button(action: {
                withAnimation {
                    self.expanded.toggle()
                }
            }) {
                Text(key)
            }
                .buttonStyle(DisclosureButtonStyle(direction: expanded ? .up : .down))
                .foregroundColor(.inputBackground)
                .frame(height: 36.0)
                .frame(maxWidth: .infinity)
                .zIndex(2)
            
            if expanded {
                VStack(alignment: .leading, spacing: 15) {
                    HStack(spacing: 15) {
                        Text("Measurement")
                        NavigationLink(destination: PublicMetricsListView(isDisplayed: $selectMeasurement, config: $config), isActive: $selectMeasurement) {
                            Text(config.metricType == nil ? "Select Measurement" : config.metricType!.rawValue)
                        }
                            .buttonStyle(DisclosureButtonStyle())
                            .foregroundColor(.inputBackground)
                            .frame(height: 36.0)
                            .frame(maxWidth: .infinity)
                    }
                    Toggle(isOn: $config.isPublic) {
                        Text("Public")
                    }
                    .disabled(config.metricType == nil)
                }
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                    .zIndex(0)
                    .transition(.move(edge: .top))
            }
        }
    }
}

private struct UpdateDeviceMetricsView: View {
    @Binding var config: [String: MetricConfig]
    var body: some View {
        VStack(spacing: 4) {
            ForEach(Array(config.keys.enumerated()), id: \.offset) { item in
                DeviceMetricCell(key: item.element, config: Binding<MetricConfig>(get: { self.config[item.element]! }, set: { self.config[item.element] = $0 }))
            }
        }
    }
}

struct UpdateDeviceView: View {
    @Binding var state: UpdateDeviceState
    @State var readQRCode: Bool = false
    @State var selectGateway: Bool = false
    @State var selectLocation: Bool = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            VStack(alignment: .leading, spacing: 2) {
                Text("Device's name")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                TextField("Type name...", text: $state.name)
                    .textFieldStyle(MainTextFieldStyle())
            }
            
            VStack(alignment: .leading, spacing: 2) {
                Text("Gateway")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                NavigationLink(destination: GatewayListView(isDisplayed: $selectGateway, gateway: $state.gateway), isActive: $selectGateway) {
                    Text(state.gateway != nil ? state.gateway!.name : "Gateway")
                }
                    .buttonStyle(DisclosureButtonStyle())
                    .foregroundColor(.inputBackground)
                    .frame(height: 36.0)
                    .frame(maxWidth: .infinity)
            }
            
            if state.gateway == nil {
                VStack(alignment: .leading, spacing: 2) {
                    Text("Key")
                        .font(.system(size: 14))
                        .foregroundColor(.white)
                    NavigationLink(destination: QRCodeReaderView(isDisplayed: $readQRCode, token: $state.key), isActive: $readQRCode) {
                        Text(state.key.isEmpty ? "Scan the QR code" : state.key)
                    }
                        .buttonStyle(DisclosureButtonStyle())
                        .foregroundColor(.inputBackground)
                        .frame(height: 36.0)
                        .frame(maxWidth: .infinity)
                }
            }
            
            VStack(alignment: .leading, spacing: 2) {
                Text("Location")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                NavigationLink(destination: LocationSelectionView(location: $state.location, isDisplayed: $selectLocation), isActive: $selectLocation) {
                    HStack(spacing: 15) {
                        TextField("Select your location", text: $state.locationDescription)
                            .textFieldStyle(MainTextFieldStyle())
                            .disabled(true)
                        Image("pin_icon")
                            .foregroundColor(.white)
                    }
                }
            }
            
            VStack(alignment: .leading, spacing: 2) {
                Text("Data format")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                Picker(selection: $state.dataFormatIndex, label: EmptyView()) {
                    ForEach(0..<DeviceDataFormat.allCases.count) {
                        Text(DeviceDataFormat.allCases[$0].displayName).frame(height: 80)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            VStack(alignment: .leading, spacing: 8) {
                Text("Metrics")
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(.white)
                UpdateDeviceMetricsView(config: $state.config)
            }
        }
    }
}
