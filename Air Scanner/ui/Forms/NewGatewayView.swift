//
//  NewGatewayView.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 29.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import SwiftUI

struct GatewayState {
    var isUpdate: Bool = false
    var name: String = ""
    var key: String = ""
    
    var isValid: Bool {
        !name.isEmpty && (isUpdate || !key.isEmpty)
    }
    
    var request: AddGatewayRequest {
        AddGatewayRequest(key: key, displayName: name)
    }
    
    mutating func clear() {
        name = ""
        key = ""
    }
}

struct NewGatewayView: View {
    @Binding var state: GatewayState
    @State var readQRCode: Bool = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            VStack(alignment: .leading, spacing: 2) {
                Text("Gateway's name")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                TextField("Type name...", text: $state.name)
                    .textFieldStyle(MainTextFieldStyle())
            }
            VStack(alignment: .leading, spacing: 2) {
                Text("Key")
                    .font(.system(size: 14))
                    .foregroundColor(.white)
                NavigationLink(destination: QRCodeReaderView(isDisplayed: $readQRCode, token: $state.key), isActive: $readQRCode) {
                    Text(state.key.isEmpty ? "Scan the QR code" : state.key)
                }
                    .buttonStyle(DisclosureButtonStyle())
                    .foregroundColor(.inputBackground)
                    .frame(height: 36.0)
                    .frame(maxWidth: .infinity)
            }
        }
    }
}
