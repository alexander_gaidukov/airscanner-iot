//
//  Geocoder.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 24.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

final class Geocoder {
    static let shared = Geocoder()
    
    @discardableResult
    func loadAddress(from loc: Location, completion: @escaping (Address?) -> ()) -> URLSessionDataTask {
        
        let queryURL =  URL(string:"https://nominatim.openstreetmap.org/reverse?format=json&lat=\(loc.lat)&lon=\(loc.lon)&addressdetails=1&limit=1")!
        
        let task = URLSession.shared.dataTask(with: queryURL) {data, response, error in
            guard error == nil else {
                completion(nil)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil)
                return
            }
            
            if (200..<300) ~= httpResponse.statusCode {
                if let value = data.flatMap({ try? Coders.snakeCaseDecoder.decode(AddressResponse.self, from: $0) }) {
                    completion(value.address)
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
        task.resume()
        return task
    }
}
