//
//  Endpoint.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 17.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

enum EndpointError: Error {
    case noInternetConnection
    case other
    case custom(String)
}

struct APIError: Decodable {
    var message: String
}

extension EndpointError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return "Internet connection has been lost."
        case .other:
            return "Something weird happend. Keep calm and try again."
        case .custom(let message):
            return message
        }
    }
}

struct Coders {
    static var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .useDefaultKeys
        return encoder
    }()
    
    static var snakeCaseEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()
    
    static var snakeCaseDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    static var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        return decoder
    }()
}

final class Endpoint<A: Decodable>: ObservableObject {
    
    @Published var result: Result<A, EndpointError>?
    
    var value: A? {
        try? result?.get()
    }
    
    private var dataTask: URLSessionDataTask?
    private var baseURL: URL = API.baseURL
    private var path: String = ""
    private var method: HTTPMethod = .get
    var encodeToSnakeCase = true
    var decodeFromSnakeCase = true
    var params: [String: Any] = [:]
    
    var isEmpty: Bool {
        path.isEmpty
    }
    
    init(baseURL: URL = API.baseURL, path: String = "", method: HTTPMethod = .get, params: [String: Any] = [:]) {
        configure(baseURL: baseURL, path: path, method: method, params: params)
    }
    
    func configure(baseURL: URL, path: String, method: HTTPMethod, params: [String: Any] = [:]) {
        self.baseURL = baseURL
        self.path = path
        self.method = method
        self.params = params
    }
    
    @discardableResult
    func load<B: Encodable>(params: B?) -> URLSessionDataTask {
        dataTask?.cancel()
        let url = baseURL.appendingPathComponent(path)
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        components.queryItems = self.params.flatMap { (key, value) -> [URLQueryItem] in
            if let arr = value as? [Any] {
                return arr.map { URLQueryItem(name: key, value: String(describing: $0)) }
            }
            return [URLQueryItem(name: key, value: String(describing: value))]
        }
        var request = URLRequest(url: components.url!)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(LocalStorage.shared.user?.idToken ?? "")", forHTTPHeaderField: "Authorization")
        
        if let params = params {
            switch method {
            case .post, .put:
                let encoder = encodeToSnakeCase ? Coders.snakeCaseEncoder : Coders.encoder
                request.httpBody = try? encoder.encode(params)
            default:
                break
            }
        }
        
        let dataTask = URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            
            let str = data.flatMap { String(data: $0, encoding: .utf8) } ?? "No data"
            print(str)
            
            guard error == nil else {
                switch (error as? URLError)?.code {
                case .cancelled?:
                    break
                case .notConnectedToInternet?, .networkConnectionLost?:
                    self?.update(with: .failure(.noInternetConnection))
                default:
                    self?.update(with: .failure(.other))
                }
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                self?.update(with: .failure(.other))
                return
            }
            
            if (200..<300) ~= httpResponse.statusCode {
                let decoder = self?.decodeFromSnakeCase == true ? Coders.snakeCaseDecoder : Coders.decoder
                if let value = data.flatMap({ try? decoder.decode(A.self, from: $0) }) {
                    self?.update(with: .success(value))
                } else if let value = try? decoder.decode(A.self, from: "{}".data(using: .utf8)!) {
                    self?.update(with: .success(value))
                } else {
                    self?.update(with: .failure(.other))
                }
            } else {
                let apiError: EndpointError = data.flatMap({ try? Coders.decoder.decode(APIError.self, from: $0) }).map { .custom($0.message) } ?? .other
                self?.update(with: .failure(apiError))
            }
        }
        dataTask.resume()
        self.dataTask = dataTask
        
        return dataTask
    }
    
    private func update(with result: Result<A, EndpointError>) {
        DispatchQueue.main.async {
            self.result = result
        }
    }
}
