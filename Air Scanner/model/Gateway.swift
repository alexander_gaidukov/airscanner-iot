//
//  Gateway.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 17.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

func ==(lhs: Gateway, rhs: Gateway) -> Bool {
    return lhs.id == rhs.id
}

final class Gateway: Hashable, ObservableObject {
    let id: String
    @Published var name: String = ""
    
    init(id: String, data: [String: Any]) {
        self.id = id
        configure(with: data)
    }
    
    func configure(with data: [String: Any]) {
        self.name = data["display_name"] as? String ?? ""
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
