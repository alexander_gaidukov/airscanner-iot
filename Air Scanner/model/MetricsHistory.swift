//
//  MetricsHistory.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 03.07.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct MetricsRecord: Decodable {
    var timestamp: Double
    var value: Double
    
    private struct CustomCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
        
        guard let key = container.allKeys.first,
            let date = Formatters.dateFormatter.date(from: key.stringValue) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: ""))
        }
        
        timestamp = date.timeIntervalSinceReferenceDate
        value = try container.decode(Double.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!)
    }
}

struct MetricsHistory: Decodable {
    var history: [String: [MetricsRecord]]
    
    var isEmpty: Bool {
        return history.isEmpty
    }
    
    private struct CustomCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        var intValue: Int?
        init?(intValue: Int) {
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CustomCodingKeys.self)
        
        var history: [String: [MetricsRecord]] = [:]
        
        for key in container.allKeys {
            let values = try container.decode(FailableDecodableArray<MetricsRecord>.self, forKey: CustomCodingKeys(stringValue: key.stringValue)!).elements
            if values.count > 1 {
                history[key.stringValue] = values
            }
        }
        
        self.history = history
    }
}
