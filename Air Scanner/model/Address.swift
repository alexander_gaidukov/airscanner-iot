//
//  Address.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 25.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct Address: Decodable {
    var country: String?
    var state: String?
    var city: String?
    var road: String?
    var houseNumber: String?
    
    var address: String? {
        if let road = road {
            return [road, houseNumber].compactMap { $0 }.joined(separator: ", ")
        }
        
        if let city = city {
            return [city, country].compactMap { $0 }.joined(separator: ", ")
        }
        
        if let country = country {
            return country
        }
        
        return nil
    }
}

struct AddressResponse: Decodable {
    var address: Address
}
