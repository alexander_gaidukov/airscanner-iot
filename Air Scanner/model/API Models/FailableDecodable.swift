//
//  FailableDecodable.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 03.07.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct FailableDecodable<Base: Decodable>: Decodable {
    let base: Base?
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        base = try? container.decode(Base.self)
    }
}
