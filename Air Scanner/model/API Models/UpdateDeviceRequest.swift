//
//  UpdateDeviceRequest.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 30.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct UpdateDeviceRequest: Encodable {
    let key: String
    let gateway_id: String
    let display_name: String
    let is_public: Bool
    let loc_lat: Float
    let loc_lon: Float
    let data_format: DeviceDataFormat
    let location_description: String
    let metricsConfig: [String: MetricConfig]
}
