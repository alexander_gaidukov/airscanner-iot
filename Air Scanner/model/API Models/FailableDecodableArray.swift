//
//  FailableDecodableArray.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 03.07.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation

struct FailableDecodableArray<Element: Decodable>: Decodable {
    private(set) var elements: [Element]
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        var elements: [Element] = []
        if let count = container.count {
            elements.reserveCapacity(count)
        }
        
        while !container.isAtEnd {
            if let element = try container.decode(FailableDecodable<Element>.self).base {
                elements.append(element)
            }
        }
        
        self.elements = elements
    }
}
