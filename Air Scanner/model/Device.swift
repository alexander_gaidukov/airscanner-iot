//
//  Device.swift
//  Air Scanner
//
//  Created by Alexandr Gaidukov on 15.06.2020.
//  Copyright © 2020 Grid dynamics. All rights reserved.
//

import Foundation
import FirebaseFirestore
import Combine

enum DeviceDataFormat: String, CaseIterable, Encodable {
    case json = "json"
    case singleValue = "single_value"
    
    var displayName: String {
        switch self {
        case .json:
            return "JSON Value"
        case .singleValue:
            return "Single Value"
        }
    }
}

typealias Location = (lat: Float, lon: Float)

extension ClosedRange where Bound == Int {
    var label: String {
        return "\(lowerBound)-\(upperBound)"
    }
}

func <(lhs: PublicMetric, rhs: PublicMetric) -> Bool {
    PublicMetric.allCases.firstIndex(of: lhs)! < PublicMetric.allCases.firstIndex(of: rhs)!
}

enum PublicMetric: String, CaseIterable, Equatable, Comparable {
    case pm2_5 = "PM2,5"
    case pm1_0 = "PM1,0"
    case pm10 = "PM10"
    case co2 = "CO2"
    case humidity = "Humidity"
    case temperature = "Temperature"
    
    var normalRange: ClosedRange<Int> {
        switch self {
        case .co2:
            return 400...2000
        case .humidity:
            return 0...100
        case .temperature:
            return -20...40
        case .pm10, .pm1_0, .pm2_5:
            return 0...150
        }
    }
}

struct MetricConfig: Encodable {
    var metricType: PublicMetric?
    var isPublic: Bool
    enum CodingKeys: String, CodingKey {
        case measurementType = "measurementType"
        case isPublic = "is_public"
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(metricType?.rawValue ?? "", forKey: .measurementType)
        try container.encode(isPublic, forKey: .isPublic)
    }
}

struct Metric {
    let value: Double
    let lastUpdate: TimeInterval
}

func ==(lhs: Device, rhs: Device) -> Bool {
    lhs.id == rhs.id
}

struct DeviceSettings {
    var displayName: String = ""
    var dataFormat: DeviceDataFormat = .singleValue
    var location: Location = (lat: 0, lon: 0)
    var publicMetrics: [PublicMetric] = []
    var config: [String: MetricConfig] = [:]
    var locationDescription: String = ""
    var gatewayId: String = ""
    var userId: String = ""
}

struct DeviceMetricItem {
    var key: String
    var metricConfig: MetricConfig?
    var value: Double
}

final class Device: Hashable, ObservableObject {
    let id: String
    @Published var settings = DeviceSettings()
    @Published var metrics: [String: Metric] = [:]
    var address: String {
        settings.locationDescription.isEmpty ? "\(settings.location.lat), \(settings.location.lon)" : settings.locationDescription
    }
    
    var listener: ListenerRegistration?
    
    var isOwnedByCurrentUser: Bool {
        settings.userId == LocalStorage.shared.user?.id
    }
    
    var clientId: String {
        "projects/gd-gcp-rnd-connected-apps/locations/europe-west1/registries/europe-registry/devices/" + (settings.gatewayId.isEmpty ? "d-\(id)" : "g-\(settings.gatewayId)")
    }
    
    var topic: String {
        "/devices/d-\(id)/events"
    }
    
    var extendedConfig: [String: MetricConfig] {
        var result = settings.config
        for key in metrics.keys {
            guard result[key] == nil else { continue }
            result[key] = MetricConfig(metricType: nil, isPublic: false)
        }
        return result
    }
    
    var sortedMetricKeys: [String] {
        let filteredMetricsKeys: [String] = self.isOwnedByCurrentUser ? metrics.keys.filter {_ in true } : metrics.keys.filter {
            guard let metricConfig = self.settings.config[$0] else { return false }
            return metricConfig.isPublic
        }
        
        return filteredMetricsKeys.sorted {
            let lhs = self.settings.config[$0]
            let rhs = self.settings.config[$1]
            
            if let l = lhs?.metricType, let r = rhs?.metricType {
                return l < r
            }
            
            if rhs != nil {
                return false
            }
            
            return true
        }
    }
    
    var metricItems: [DeviceMetricItem] {
        sortedMetricKeys.map {
            DeviceMetricItem(key: $0, metricConfig: self.settings.config[$0], value: self.metrics[$0]?.value ?? 0)
        }
    }
    
    deinit {
        listener?.remove()
        listener = nil
    }
    
    init(id: String, data: [String: Any]) {
        self.id = id
        self.configure(with: data)
        self.listener = Firestore.firestore().collection("devices").document(id).collection("metrics").addSnapshotListener {[weak self] snapshot, _ in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents {
                let data = document.data()
                guard let value = data["value"] as? Double,
                    let timestamp = (data["last_update"] as? String).flatMap({ Double($0) }) else { continue }
                let metric = Metric(value: value, lastUpdate: timestamp)
                self?.metrics[document.documentID] = metric
            }
        }
    }
    
    func configure(with data: [String: Any]) {
        let displayName = data["display_name"] as? String ?? ""
        let dataFormat = (data["data_format"] as? String).flatMap(DeviceDataFormat.init) ?? .singleValue
        let location = (data["location"] as? GeoPoint).map { (lat: Float($0.latitude), lon: Float($0.longitude)) } ?? (lat: 0, lon: 0)
        let publicMetrics = (data["publicMetrics"] as? [String] ?? []).compactMap(PublicMetric.init)
        let locationDescription = data["location_description"] as? String ?? ""
        let gatewayId = data["gateway_id"] as? String ?? ""
        let userId = data["user_id"] as? String ?? ""
        
        let metricConfig: [String: MetricConfig]
        
        if let metricsConfig = data["metricsConfig"] as? [String: Any] {
            var config: [String: MetricConfig] = [:]
            
            for (key, value) in metricsConfig {
                guard let json = value as? [String: Any],
                    let metric = json["measurementType"] as? String else { continue }
                let isPublic = json["is_public"] as? Bool ?? false
                config[key] = MetricConfig(metricType: PublicMetric(rawValue: metric), isPublic: isPublic)
            }
            
            metricConfig = config
        } else {
            metricConfig = [:]
        }
        
        self.settings = DeviceSettings(displayName: displayName, dataFormat: dataFormat, location: location, publicMetrics: publicMetrics, config: metricConfig, locationDescription: locationDescription, gatewayId: gatewayId, userId: userId)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
